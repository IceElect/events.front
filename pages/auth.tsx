import React, { useEffect, useState } from "react";
import Head from 'next/head'
import moment from "moment";
import AuthOtpSend from "../components/AuthOtpSend";
import AuthOtpVerify from "../components/AuthOtpVerify";
import MainLayout from "../components/MainLayout";
import { authSendOTP, authVerifyOTP } from "../lib/api";
import { authProcess, getError } from "../store/user.slice";
import { useDispatch, useSelector } from "react-redux";
import useUser from "../lib/hooks/useUser";

export default function AuthPage() {
    const { user, isLoading, isLogged } = useUser({redirectTo: '/profile', redirectIfFound: true});

    const [phone, setPhone] = useState(null);
    const [verify, setVerify] = useState(false);
    const [lastSentAt, setLastSentAt] = useState(0);
    const [duration, setDuration] = useState(null);

    const dispatch = useDispatch();
    const error = useSelector(getError)

    const sendSubmit = async (data) => {
        const otp = await sendOTP(data.phone);
        return true;
    }

    const sendOTP = async (phone) => {
        const otp = await authSendOTP(phone);
        if(otp) {
            setPhone(phone);
            setVerify(true);
            setDuration(60);
        } else {
            alert('Произошла ошибка при отправке смс');
        }
    }

    const resend = async () => {
        const otp = await sendOTP(phone);
    }

    const verifySubmit = async (data) => {
        const result = await authVerifyOTP(phone, data.code);
        dispatch(authProcess(result));
    }

    useEffect(() => {
        setInterval(() => {
            setDuration(d => d - 1);
        }, 1000)
    }, [])

    if(isLogged) {
        return <></>
    }

    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Вход в аккаунт</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            {verify
                ? <AuthOtpVerify 
                    phone={phone} 
                    setVerify={setVerify}
                    onSubmit={verifySubmit}
                    resend={resend}
                    duration={duration}
                    error={error}
                />
                : <AuthOtpSend phone={phone} onSubmit={sendSubmit} />
            }
        </MainLayout>
    )
}