import Head from 'next/head';
import React from "react"
import MainLayout from "../../components/MainLayout"
import PageHeader from "../../components/PageHeader/PageHeader"
import Container from "../../components/UI/Container"
import nookies from "nookies";
import { getMyTickets, setToken } from '../../lib/api';

export async function getServerSideProps(ctx) {
    const { res } = ctx
    const token = nookies.get(ctx).token;
    if(!token) {
        res.writeHead(301, { Location: '/auth' })
        res.end()
    }
    setToken(token);
    const tickets: any = await getMyTickets();
    const ticket = tickets.data.find(t => t.id == ctx.params.id);

    // if(ticket?.status == "paid") {
    //     res.writeHead(301, { Location: '/profile' })
    //     res.end()
    // }

    return {
        props: {...ticket}
    }
}

export default function PaymentPage({id, ticket}) {
    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Оплата билета &quot;{id}&quot;</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Container>
                <PageHeader>Оплата билета №{id}</PageHeader>
                <h3>Пожалуйста, переведите {ticket.price}₽ по указанным ниже реквизитам:</h3>
                <p>После того, как перевод будет выполнен, мы отметим ваш билет как оплаченный.</p>
                <p>Информация о билете будет в вашем личном кабинете и в сообщении на электронной почте</p>
                <p>
                    <b> По номеру телефона:</b><br/> 
                    +79636645225 Сбер/Тинькофф/Киви<br/><br/>
                    <b>По номеру карты:</b><br/>
                    2202 2013 3819 5056 - Сбер<br/>
                    5536 9138 0478 7840 - Тинькофф<br/>
                </p>
                <div className="block">В комментарии к платежу необходимо указать &quot;Оплата билета №{id}&quot;</div>
            </Container>
        </MainLayout>
    )
}