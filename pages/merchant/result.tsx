import Head from 'next/head';
import React from "react"
import MainLayout from "../../components/MainLayout"
import PageHeader from "../../components/PageHeader/PageHeader"
import Container from "../../components/UI/Container"
import bodyParser from "body-parser";
import { promisify } from "util";

const getBody = promisify(bodyParser.urlencoded());

export async function getServerSideProps(ctx) {
    const { req, res } = ctx
    if (req.method !== 'POST') return {notFound: true}
    await getBody(req, res);

    return {props: req.body}
}

export default function PaymentPage({custom: name}) {
    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Оплата прошла успешно</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Container>
                <PageHeader>Оплата прошла успешно</PageHeader>
                <p>Оплата билета {name} прошла успешно. Спасибо за покупку.</p>
            </Container>
        </MainLayout>
    )
}
