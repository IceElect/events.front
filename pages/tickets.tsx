import Head from 'next/head';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../components/Loader/Loader';
import MainLayout from "../components/MainLayout";
import BoxTicket from "../components/BoxTicket/BoxTicket";
import PageHeader from "../components/PageHeader/PageHeader";
import Container from "../components/UI/Container";
import useUser from "../lib/hooks/useUser";
import { fetchTickets, getTickets } from '../store/user.slice';

export default function Tickets({}) {
    const dispatch = useDispatch();
    const tickets = useSelector(getTickets);
    const { user, isLoading, isLogged, isFilled } = useUser({redirectTo: '/auth'});

    useEffect(() => {
        if(!isLoading && !tickets)
            dispatch(fetchTickets());
    }, [isLoading, tickets])

    if(!isLogged) {
        return <></>
    }

    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Ваши билеты</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Container>
                <PageHeader>Ваши билеты</PageHeader>
                {!isLoading 
                    ? tickets ? <div>
                        {tickets.map(ticket => <BoxTicket key={ticket.id} {...ticket} />)}
                    </div> : null
                    : <Loader />
                }
            </Container>
        </MainLayout>
    )
}