import Head from 'next/head';
import React from "react";
import MainLayout from "../../components/MainLayout";
import PageHeader from "../../components/PageHeader/PageHeader";
import Container from "../../components/UI/Container";

export default function Contacts() {
    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Контакты</title>
                <meta name="description" content="SL.Events - Контакты" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Container>
                <PageHeader>Контакты</PageHeader>
                <p style={{display: 'flex', alignItems: 'center'}}>
                    <i style={{paddingRight: 10}} className="fa fa-2x fa-fw fa-phone"></i>
                    <a href="callto:89636645225">+79636645225</a>
                </p>
                <p style={{display: 'flex', alignItems: 'center'}}>
                    <i style={{paddingRight: 10}} className="fa fa-2x fa-fw fa-envelope"></i>
                    <a href="mailto:events@slto.ru">events@slto.ru</a>
                </p>
            </Container>
        </MainLayout>
    )
}