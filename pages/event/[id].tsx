import moment from 'moment';
import Head from 'next/head';
import Link from 'next/link';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import BoxNew from '../../components/BoxNew/BoxNew';
import MainLayout from "../../components/MainLayout";
import Button from '../../components/UI/Button';
import Container from '../../components/UI/Container';
import { getEvent } from "../../lib/api";
import useUser from '../../lib/hooks/useUser';

import styles from "../../styles/event.module.scss";

export async function getServerSideProps({params}) {
    const event: any = await getEvent(params.id);

    if(!event) {
        return {
            notFound: true,
        }
    }

    return {
        props: event
    }
}

export default function EventPage({cover, status, title, description, organizers, articles, hold_map, tickets, location, start_at, end_at}) {
    const { user, isLoading, isLogged, isFilled } = useUser();

    return (
        <MainLayout>
            <Head>
                <title>SL.Events - {title}</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className={styles.topLine}>
                <Container>
                    <Link href="/">
                        <a className={styles.topLine__arrow}>
                            <i className="fal fa-arrow-left"></i>
                        </a>
                    </Link>
                    <span className={styles.topLine__category}>Мероприятия</span>
                    <span className={styles.topLine__title}>{title}</span>
                    <div className={styles.topLine__logo}>
                        <img src="/logo.png" alt="SL,Events" />
                    </div>
                </Container>
            </div>
            <Container>
                <div className={styles.cover}>
                    <img src={cover} alt={title} />
                </div>
                <div className={styles.meta}>
                    <div className={styles.meta__item}>
                        <div className={styles.meta__icon}><i className="fal fa-fw fa-map-marker-alt"></i></div>
                        <div className={styles.meta__data}>
                            <div>{location.address}</div>
                            <span>{location.latitude}, {location.longitude}</span>
                        </div>
                    </div>
                    <div className={styles.meta__item}>
                        <div className={styles.meta__icon}><i className="fal fa-fw fa-clock"></i></div>
                        {status === 'release' && <div className={styles.meta__data}>
                            <div><b>Начало:</b> {moment(start_at).locale('ru').format('LL в HH:mm')}</div>
                            <span>До начала осталось {moment(start_at).diff(moment(), 'days')} дня</span>
                        </div>}
                        {status === 'ended' && <div className={styles.meta__data}>
                            <div><b>Окончено</b> {moment(end_at).locale('ru').format('LL в HH:mm')}</div>
                        </div>}
                    </div>
                </div>
                <div className={styles.holder}>
                    <section className={styles.section}>
                        <div className={styles.section__header}>Анонс</div>
                        <div className={styles.section__content}>
                            <ReactMarkdown>{description}</ReactMarkdown>
                        </div>
                    </section>
                    {organizers && <section className={styles.section}>
                        <div className={styles.section__header}>Организаторы</div>
                        <div className={styles.section__content}>
                            <div className={styles.organizer__list}>
                                {organizers.map((organizer => (
                                    <div className={styles.organizer} key={organizer.id}>
                                        <div className={styles.organizer__avatar}>
                                            <img src={organizer.avatar} alt={organizer.name} />
                                        </div>
                                        <div className={styles.organizer__name}>{organizer.name}</div>
                                        <div className={styles.organizer__role}>{organizer?.pivot?.role}</div>
                                    </div>
                                )))}
                            </div>
                        </div>
                    </section>}

                    <section className={styles.section}>
                        <div className={styles.section__header}>Билеты</div>
                        <div className={styles.section__content}>
                            <div className={styles.ticket__list}>
                                {tickets.map(ticket => (
                                    <div className={styles.ticket} key={ticket.id}>
                                        <div className={styles.ticket__name}>{ticket.name}</div>
                                        <div className={styles.ticket__remaining}>Осталось <b>{ticket.remaining}</b> шт</div>
                                        <div className={styles.ticket__description}>{ticket.description}</div>
                                        {(ticket.remaining && +new Date(end_at) > Date.now()) 
                                            ? <Link href={isLogged ? `/ticket/${ticket.id}/buy` : '/auth'}>
                                                <Button as="a" theme="main" className={styles.ticket__button}>{ticket.price} Р</Button>
                                            </Link>
                                            : <Button as="a" theme="main" className={styles.ticket__button} disabled={true}>{ticket.price} Р</Button>
                                        }
                                        
                                    </div>
                                ))}
                            </div>
                        </div>
                    </section>

                    {hold_map && <section className={styles.section}>
                        <div className={styles.section__header}>Карта зала</div>
                        <div className={styles.section__content}>
                            <img src={hold_map} style={{display: 'block', width: '100%', marginTop: 10, borderRadius: 20}} alt="Карта помещения" />
                        </div>
                    </section>}

                    {articles && <section className={styles.section}>
                        <div className={styles.section__header}>Новости</div>
                        <div className={styles.section__content}>
                            {articles.map(article => <BoxNew
                                key={article.id}
                                source={title}
                                date={moment(article.created_at).locale('ru').format('LL')}
                                {...article}
                            />)}
                        </div>
                    </section>}
                </div>
            </Container>
        </MainLayout>
    )
}
