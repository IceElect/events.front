import { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import { store } from '../store'
import '../styles/globals.scss'
import 'moment/locale/ru';
import React from 'react';
import LoadingLayout from '../components/LoadingLayout';

function MyApp({ Component, pageProps } : AppProps) {
  return (
      <Provider store={store}>
          <LoadingLayout>
            <Component {...pageProps} />
          </LoadingLayout>
      </Provider>
  )
}

export default MyApp
