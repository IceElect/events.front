import moment from 'moment';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import MainLayout from "../../../components/MainLayout";
import PageHeader from '../../../components/PageHeader/PageHeader';
import Button from '../../../components/UI/Button';
import Container from '../../../components/UI/Container';
import { buyTicket } from "../../../lib/api";

import styles from "../../../styles/event.module.scss";
import Loader from '../../../components/Loader/Loader';
import Router from "next/router";
import Control from "../../../components/UI/Control";

// export const getStaticPaths: GetStaticPaths<{ slug: string }> = async () => {

//     return {
//         paths: [], //indicates that no page needs be created at build time
//         fallback: 'blocking' //indicates the type of fallback
//     }
// }

export async function getServerSideProps(context) {
    // setToken(nookies.get(context).token);
    // const response = await buyTicket(context.params.id);

    // if ('message' in response) {
    //     return {
    //         redirect: {
    //             permanent: false,
    //             destination: "/auth",
    //         },
    //     };
    // }

    // if(!response) {
    //     return {
    //         notFound: true,
    //     }
    // }

    // return {
    //     props: response.data
    // }

    return {props: {
        ticketId: context.params.id
    }}
}

export default function BuyPage({ticketId}) {
    const [step, setStep] = useState(0)
    const [error, setError] = useState('');
    const [ticket, setTicket] = useState(null)
    const [paymentUrl, setPaymentUrl] = useState(null)
    const [promocode, setPromocode] = useState('');

    useEffect(() => {
        if (!step) return;

        buyTicket(ticketId, promocode)
        .then(response => {
            if ('message' in response) {
                // location.href = '/auth'
                Router.push('/auth')
            } else if (response.success === false) {
                setStep(0);
                setError('Указанный промокод не существует');
            } else {
                setTicket(response.data?.ticket);
                setPaymentUrl(response.data?.payment_url)
            }
        });
    }, [step])

    if (!step) {
        return (
          <MainLayout>
              <Head>
                  <title>SL.Events - Покупка билета</title>
                  <meta name="description" content="SL.Events - Лучшие мероприятия" />
                  <link rel="icon" href="/favicon.ico" />
              </Head>
              <Container>
                  <PageHeader>Введите промокод</PageHeader>
                  <Control name="promocode" placeholder="Промокод" value={promocode} onChange={event => setPromocode(event.target.value)} error={error} />
                  <div style={{ display: 'flex', gap: '20px' }}>
                      <Button as="button" onClick={() => setStep(1)} theme="primary" full>Пропустить</Button>
                      <Button as="button" onClick={() => setStep(1)} theme="main" full>Перейти к оплате</Button>
                  </div>
              </Container>
          </MainLayout>
        )
    }

    if (!ticket) return <Loader />
    const event = ticket.event;

    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Покупка билета</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Container>
                <PageHeader>Билет забронирован</PageHeader>
                <div className={styles.ticket} key={ticket.id}>
                    <div className={styles.ticket__name}>{ticket.name}</div>
                    {/* <div className={styles.ticket__remaining}>Осталось <b>{remaining}</b> шт</div> */}
                    <div className={styles.ticket__description}>{ticket.description}</div>
                    {/* <Button theme="main" className={styles.ticket__button} disabled={!ticket.remaining}>{ticket.price} Р</Button> */}
                </div>
                {/* <section className={styles.section}>
                    <div className={styles.section__header}>{event.title}</div>
                    <div className={styles.section__content}></div>
                </section> */}
                <div className={styles.meta} style={{marginTop: 30}}>
                    <div className={styles.meta__item}>
                        <div className={styles.meta__icon}><i className="fal fa-fw fa-map-marker-alt"></i></div>
                        <div className={styles.meta__data}>
                            <div>{event.location.address}</div>
                            <span>{event.location.latitude}, {event.location.longitude}</span>
                        </div>
                    </div>
                    <div className={styles.meta__item}>
                        <div className={styles.meta__icon}><i className="fal fa-fw fa-clock"></i></div>
                        <div className={styles.meta__data}>
                            <div><b>Начало:</b> {moment(event.start_at).locale('ru').format('LL в hh:mm')}</div>
                            <span>До начала осталось {moment(event.start_at).diff(moment(), 'days')} дня</span>
                        </div>
                    </div>
                </div>
                <section className={styles.section}>
                    <div className={styles.section__header} style={{fontSize: 24}}>К оплате: {promocode ? 'Указано на странице оплаты' : `${ticket.price} ₽`}</div>
                    <div className={styles.section__content}>
                        Ваш билет успешно забронирован. Пожалуйста оплатите билет в течении 20 минут, иначе ваше бронирование отменится и билет может купить кто-нибудь другой.
                    </div>
                </section>
                <Button as="a" href={paymentUrl} theme="main" full>Перейти к оплате</Button>
            </Container>
        </MainLayout>
    )
}
