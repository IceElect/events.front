import moment from 'moment'
import Head from 'next/head'
import Image from 'next/image'
import nookies from 'nookies'
import React, { useState } from 'react'
import BoxEvent from '../components/BoxEvent/BoxEvent'
import BoxNew from '../components/BoxNew/BoxNew'
import MainLayout from '../components/MainLayout'
import PageHeader from '../components/PageHeader/PageHeader'
import Tabs from '../components/Tabs/Tabs'
import Button from '../components/UI/Button'
import Container from '../components/UI/Container'
import { getArticles, getEvents, getToken } from '../lib/api'

export async function getServerSideProps(context) {
  const events: any = await getEvents();
  const articles: any = await getArticles();

  return {
    props: {
      events: events.data,
      articles: articles.data
    }
  }
}

export default function Home({articles, events}) {
  console.log(getToken());
  const [currentTab, setCurrentTab] = useState<string>('events')

  const tabs = [
    {
      index: 'events',
      label: 'События',
    },
    {
      index: 'articles',
      label: 'Новости',
    },
    // {
    //   index: 'calendar',
    //   label: 'Календарь'
    // }
  ];

  return (
    <MainLayout>
      <Head>
        <title>SL.Events - Лучшие мероприятия</title>
        <meta name="description" content="SL.Events - Лучшие мероприятия" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container>
        <PageHeader>SL.Events</PageHeader>
        <div style={{display: 'flex'}}>
          <Button style={{marginRight: 15}}><i className="fal fa-search"></i></Button>
          <div style={{flex: 1}}>
            <Tabs 
              tabs={tabs} 
              current={currentTab}
              setCurrent={setCurrentTab}
            />
          </div>
        </div>
        {currentTab == 'articles' ? <Articles articles={articles}/> : < Events events={events}/>}
      </Container>
    </MainLayout>
  )
}

const Articles = ({articles}) =>  (
  <>
    {articles.map(article => <BoxNew 
      key={article.id}
      {...article}
      date={moment(article.created_at).locale('ru').format('LL')}
      source={article.event.title}
    />)}
  </>
)

const Events = ({events}) => (
  <div style={{display: 'flex', flexWrap: 'wrap', marginLeft: -15, marginRight: -15}}>
    {events.map(event => <BoxEvent key={event.id} {...event} />)}
  </div>
)