import Head from 'next/head';
import BoxProfile from "../components/BoxProfile/BoxProfile";
import MainLayout from "../components/MainLayout";
import PageHeader from "../components/PageHeader/PageHeader";
import Container from "../components/UI/Container";
import useUser from "../lib/hooks/useUser";

export default function ProfilePage() {
    const { user, isLoading, isLogged, isFilled } = useUser({redirectTo: '/auth'});

    if(!isLogged) {
        return <></>
    }

    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Ваш профиль</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Container>
                <PageHeader>Ваш профиль</PageHeader>
                <BoxProfile {...user} />
            </Container>
        </MainLayout>
    )
}