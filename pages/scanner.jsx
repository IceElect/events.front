import dynamic from 'next/dynamic'

const DimamycScanner = dynamic(
    () => import('../components/Scanner/Scanner'),
    { ssr: false }
)

export default function Scanner() {
    return <DimamycScanner />
}