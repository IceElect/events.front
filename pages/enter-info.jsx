import Head from 'next/head';
import MainLayout from '../components/MainLayout';
import Control from "../components/UI/Control";
import Button from "../components/UI/Button";
import { useForm, useFormState } from "react-hook-form";
import styles from "../styles/auth.module.scss";
import useUser from '../lib/hooks/useUser';
import { useDispatch, useSelector } from 'react-redux';
import { editProfile, getError } from '../store/user.slice';
import { useEffect } from 'react';
import Router from 'next/router';

export default function EnterInfoPage() {

    const dispatch = useDispatch();
    const error = useSelector(getError);
    const { user, isLogged, isFilled } = useUser({redirectIfNotFilled: false});

    const { register, handleSubmit, setValue, control } = useForm({
        mode: 'all',
        reValidateMode: 'onBlur',
    });
    const { isSubmitting, isValid, errors } = useFormState({control});

    const onSubmit = async (data) => {
        await dispatch(editProfile(data));
    }

    useEffect(() => {
        if(isLogged) {
            setValue('first_name', user.first_name);
            setValue('last_name', user.last_name);
            setValue('email', user.email);
        }
    }, [user, isLogged])

    useEffect(() => {
        if(isFilled) {
            Router.push('/profile');
        }
    }, [isFilled])

    return (
        <MainLayout>
            <Head>
                <title>SL.Events - Ещё немного...</title>
                <meta name="description" content="SL.Events - Лучшие мероприятия" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <form onSubmit={handleSubmit(onSubmit)} className={styles.authForm}>
                <h1 className={styles.header}>
                    <span>Ещё немного...</span>
                    {/* <span className={styles.header__edit} onClick={() => setVerify(false)}><i className="fas fa-pencil"/></span> */}
                </h1>
                <p className={styles.header__desc}>Укажите информацию о себе, чтобы мы лучше знали своих гостей</p>
                <Control 
                    name="firstName"
                    placeholder="Ваше имя"
                    {...register("first_name", { required: true, minLength: 2 })} 
                    error={errors?.code?.message}
                />
                <Control 
                    name="lastName"
                    placeholder="Ваша фамилия"
                    {...register("last_name", { required: true, minLength: 3 })} 
                    error={errors?.code?.message}
                />
                <Control 
                    name="email"
                    placeholder="Ваша электронная почта"
                    {...register("email", { required: true, minLength: 5 })} 
                    error={errors?.code?.message}
                />
                {error && <div className="error">{error}</div>}
                <Button full={1} theme="main" disabled={!isValid} loading={isSubmitting}>Продолжить</Button>
            </form>
        </MainLayout>
    )
}