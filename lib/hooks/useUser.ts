import { useEffect } from 'react'
import Router from 'next/router'
import { useSelector } from 'react-redux'
import { getError, getIsLoading, getIsLogged, getUser } from '../../store/user.slice'

export default function useUser({
  redirectTo = null,
  redirectIfFound = false,
  redirectIfNotFilled = true,
} = {}) {
    const user = useSelector(getUser);
    const error = useSelector(getError);
    const isLogged = useSelector(getIsLogged);
    const isLoading = useSelector(getIsLoading) || user === null;

    const finished = Boolean(isLoading);
    const hasUser = Boolean(user?.id);
    const isFilled = Boolean(user?.first_name) && Boolean(user?.last_name) && Boolean(user?.email);

    useEffect(() => {
      if(user === null) return;

      if(user === false && redirectTo) {
        Router.push('/auth')
      }

      if (hasUser && !isFilled && redirectIfNotFilled) {
        Router.push('/enter-info')
      }
      
      if (!redirectTo) return
      if (
        (redirectTo && !redirectIfFound && user === false) 
        || (redirectIfFound && hasUser)
      ) {
        Router.push(redirectTo)
      }
    }, [redirectTo, redirectIfFound, finished, hasUser])
  
    return {user, isLoading, isLogged, isFilled}
}