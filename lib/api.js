import axios from 'axios';
import nookies from 'nookies'

export const baseURL = 'https://api.events.slto.ru/api';
// export const baseURL = 'http://localhost:8000/api';

var token = null;

export const getToken = () => token || nookies.get(null).token;
export const setToken = (t) => {
    token = t;
    nookies.set(null, 'token', t)
};
export const freshToken = () => nookies.destroy(null, 'token');

export const api = async (method, endpoint, params) => {
    try {
        const token = getToken();
        const response = await axios({
            method,
            [method?.toLowerCase() === 'get' ? 'params' : 'data']: params,
            url: baseURL + endpoint,
            headers: {
                common: {
                    Authorization: `Bearer ${token}`
                }
            }
        });

        return response.data;
    } catch (error) {
        if(error.response) {
            const { status, data } = error.response;

            if(status === 401 && typeof window !== 'undefined') {
                freshToken();
                // window.location.refresh();
            }

            return data;
        } else {
            console.log(error);
        }
        // alert('Unauthorized');
        // alert(error?.data?.errors?.file[0]);
    }
}

export const authSendOTP = async (phone) => {
    return await api('post', '/auth/otp/send', {
        phone: phone.replace(/\s/g, '')
    });
}

export const authVerifyOTP = async (phone, code) => {
    return await api('post', '/auth/otp/verify', {
        code: code.replace(/\s/g, ''),
        phone: phone.replace(/\s/g, ''),
    });
}

export const getProfile = async () => {
    return await api('get', '/profile');
}

export const getMyTickets = async () => {
    return await api('get', '/profile/tickets');
}

export const updateProfile = async (data) => {
    return await api('put', '/profile', data);
}

export const getArticles = async () => {
    return await api('get', '/articles');
}

export const getEvents = async () => {
    return await api('get', '/events');
}

export const getEvent = async (id) => {
    return await api('get', `/events/${id}`);
}

export const getTicket = async (id) => {
    return await api('get', `/tickets/${id}`);
}

export const buyTicket = async (id, promocode) => {
    return await api('post', `/tickets/${id}/buy`, {promocode});
}

export const findUserTicket = async (hash) => {
    return await api('get', `/ticket-user/${hash}/find`);
}

export const statusUserTicket = async (id, status) => {
    return await api('patch', `/ticket-user/${id}/status`, {status});
}

export const merchStatusUserTicket = async (id, merch_status) => {
    return await api('patch', `/ticket-user/${id}/merch_status`, {merch_status});
}
