import moment from 'moment';
import Link from 'next/link';
import React from 'react';
import Button from '../UI/Button';
import styles from './BoxTicket.module.scss';

export default function BoxTicket({id, status, payment_url, used_at, qr, ticket}) {
    const event = ticket.event;
    return (
        <div className={styles.holder}>
            <div className={styles.name}><Link href={`/event/${event.id}`}><a>{event.title}</a></Link> - {ticket.name}</div>
            <div className={styles.description}>{ticket.description}</div>
            <ul className={styles.meta}>
                <li><b>Номер билета:</b> №{id}</li>
                <li><b>Стоимость:</b> {ticket.price} ₽</li>
                {/* <li><b>Дата использования:</b> {used_at ? moment(used_at).locale('ru').format('LLL') : '—'}</li> */}
            </ul>
            <div className={styles.actions}>
                {status == "booked" && <Button as="a" href={payment_url} theme="main" className={styles.button}>Перейти к оплате</Button>}
                {status == "paid" && <Button className={styles.button}>Оплачен</Button>}
                {status == "used" && <Button className={styles.button}>Использован</Button>}
            </div>
            <div className={styles.qr}>
                <img src={qr} alt={ticket.name} />
            </div>
        </div>
    )
}
