import QrReader from 'modern-react-qr-reader'
import { useState } from 'react';
import { findUserTicket, merchStatusUserTicket, statusUserTicket } from '../../lib/api';
import Button from '../UI/Button';
import styles from './Scanner.module.scss';

export default function Scanner() {
    const [ticket, setTicket] = useState(null)

    const handleScan = async (data) => {
        if (data) {
            const t = await findUserTicket(data);
            if(!t.id) {
                alert('Билет не найден.');
                return false;
            } else if (t?.status == 'booked') {
                alert('Билет не оплачен.');
                return false;
            } else if (t?.status == 'used') {
                // alert('Билет уже использован.');
                // return false;
            }
            setTicket(t);
        }
    }

    const handleError = err => {
        console.error(err)
    }

    const handleUse = async () => {
        await statusUserTicket(ticket.id, 'used');
        setTicket(null);
    }

    const handleMerch = async (merch_status) => {
        await merchStatusUserTicket(ticket.id, merch_status);
        setTicket(t => ({...t, merch_status}));
    }
    
    return (
        <div>
            {ticket ? <div className={styles.info}>
                <div className={styles.row}>
                    <div className={styles.label}>ID</div>
                    <div className={styles.value}>{ticket?.id}</div>
                </div>
                <div className={styles.row}>
                    <div className={styles.label}>Покупатель</div>
                    <div className={styles.value}>{ticket?.user?.name}</div>
                </div>
                <div className={styles.row}>
                    <div className={styles.label}>Мероприятие</div>
                    <div className={styles.value}>{ticket?.ticket?.event?.title}</div>
                </div>
                <div className={styles.row}>
                    <div className={styles.label}>Тип билета</div>
                    <div className={styles.value}>{ticket?.ticket?.name}</div>
                </div>
                <div className={styles.row}>
                    <div className={styles.label}>Описание билета</div>
                    <div className={styles.value}>{ticket?.ticket?.description}</div>
                </div>
                <div className={styles.row}>
                    <div className={styles.label}>Стоимость билета</div>
                    <div className={styles.value}>{ticket?.ticket?.price} Р</div>
                </div>
                <div className={styles.row}>
                    <div className={styles.label}>Комментарий</div>
                    <div className={styles.value}>{ticket?.comment}</div>
                </div>
                <Button onClick={handleUse} theme="main" style={{marginTop: 10}} disabled={ticket?.status != 'paid'} full>Билет использован</Button>
                <Button onClick={() => setTicket(null)} style={{marginTop: 10}} full>Отмена</Button>
                <Button theme="main" onClick={() => handleMerch(true)} style={{marginTop: 10}} disabled={ticket?.merch_status} full>Мерч выдан</Button>
                {/* {ticket?.merch_status
                    ? <Button theme="second" onClick={() => handleMerch(false)} full>Мерч не выдан</Button>
                    : <Button theme="main" onClick={() => handleMerch(true)} full>Мерч выдан</Button>
                } */}
            </div> : <>
                {/* <Button onClick={() => handleScan('29568ec24152553b76b4c24fbef83e4a')}>Test</Button> */}
                <QrReader
                    delay={300}
                    facingMode={"environment"}
                    onError={handleError}
                    onScan={handleScan}
                    style={{ width: '100%' }}
                />
            </>}
        </div>
    )
}