import React from 'react';
import Link from 'next/link';
import styles from './Footer.module.scss';

export default function Footer() {
    return (
        <footer className={styles.footer}>
            <div className="container">
                <div className={styles.footer__menu}>
                    <Link href="/politics/aggrements"><a className={styles['footer__menu-link']}>Пользовательское соглашение</a></Link>
                    <Link href="/politics/privacy"><a className={styles['footer__menu-link']}>Политика конфиденциальности</a></Link>
                    <Link href="/politics/contacts"><a className={styles['footer__menu-link']}>Контакты</a></Link>
                </div>
            </div>
        </footer>
    );
}