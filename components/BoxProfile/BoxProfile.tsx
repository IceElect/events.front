import { useDispatch } from 'react-redux';
import { logout } from '../../store/user.slice';
import Button from '../UI/Button';
import styles from './BoxProfile.module.scss';

export default function BoxProfile({avatar, first_name, last_name, phone, email}) {

    const dispatch = useDispatch();

    return (
        <div className={styles.holder}>
            <div className={styles.avatar}>
                {avatar ? <img src={avatar} alt={`${first_name} ${last_name}`} /> : <span>A</span>}
            </div>
            <div className={styles.name}>{first_name} {last_name}</div>
            <div className={styles.phone}><i className="far fa-fw fa-phone"></i> {phone}</div>
            <div className={styles.email}><i className="far fa-fw fa-envelope"></i> {email}</div>
            <div className={styles.actions}>
                <Button icon="pencil" style={{marginRight: 20}} disabled={true}>Редактировать</Button>
                <Button icon="sign-out" theme="primary" onClick={() => dispatch(logout())}>Выйти из аккаунта</Button>
            </div>
        </div>
    )
}