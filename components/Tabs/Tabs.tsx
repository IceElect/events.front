import classNames from 'classnames';
import { Dispatch, SetStateAction } from 'react';
import styles from './Tabs.module.scss';

export type Tab = {
    index: string;
    label: string;
    onClick?: () => void;
}

export type TabsProps = {
    tabs: Tab[];
    current: string;
    setCurrent: Dispatch<SetStateAction<string>>;
}

export default function Tabs({tabs, current, setCurrent} : TabsProps) {

    const onTab = (tab) => {
        setCurrent(tab.index);
        if(tab.onClick) 
            tab.onClick();
    }

    return (
        <div className={styles.holder}>
            {tabs.map(tab => (
                <div 
                    key={tab.index}
                    className={classNames({
                        [styles.tab]: true,
                        [styles['tab--selected']]: current === tab.index
                    })}
                    onClick={() => onTab(tab)}
                >{tab.label}</div>
            ))}
        </div>
    )
}