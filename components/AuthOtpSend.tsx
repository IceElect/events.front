import Control from "./UI/Control";
import Button from "./UI/Button";
import styles from "../styles/auth.module.scss";
import InputMask from 'react-input-mask';
import { useForm, useFormState } from "react-hook-form";

export default function AuthOtpSend({phone, onSubmit}) {

    const { register, handleSubmit, control } = useForm({
        mode: 'all',
        reValidateMode: 'onBlur',
    });
    const { isSubmitting, isValid, errors } = useFormState({control});

    return (
        <form onSubmit={handleSubmit(onSubmit)} className={styles.authForm}>
            <h1>Войти в аккаунт</h1>
            <p style={{color: '#666', marginBottom: 18, marginTop: -10, fontSize: 14, lineHeight: 1.6}}>Пожалуйста, укажите ваш номер телефона</p>
            <Control 
                name="phone"
                as={InputMask} 
                mask="+7 999 999 9999" 
                maskChar={null}
                defaultValue={phone}
                placeholder="Номер телефона"
                message="Мы отправим на него СМС с кодом подтверждения"
                {...register("phone", { required: true, minLength: 15 })} 
                error={errors?.phone?.message}
            />
            <span className="control__message" style={{marginBottom: 15, marginTop: -5}}>Подтверждая номер телефона вы принимаете условия Пользовательского соглашения и политики конфиденциальности</span>
            {/* {errors?.phone} */}
            <Button full={1} theme="main" disabled={!isValid} loading={isSubmitting}>Получить код</Button>
        </form>
    )
}