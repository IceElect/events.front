import Footer from "./Footer/Footer";
import Header from "./Header";

export default function MainLayout({children}) {
    return (
        <div className="main">
            <Header />
            <div className="main__content">
                {children}
            </div>
            <Footer />
        </div>
    )
}