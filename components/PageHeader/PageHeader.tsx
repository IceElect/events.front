import styles from './PageHeader.module.scss';

export default function PageHeader({children}) {
    return (
        <div className={styles.holder}>
            <h1 className={styles.title}>{children}</h1>
        </div>
    )
}