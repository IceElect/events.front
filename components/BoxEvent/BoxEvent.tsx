import classNames from 'classnames';
import moment from 'moment';
import Link from 'next/link';
import styles from './BoxEvent.module.scss';
import _ from 'lodash';

export default function BoxEvent({id, cover, title, location, status, places, places_remaining, start_at, tickets}) {
    const firstTicket = _.orderBy(tickets, t => +t.price, 'asc').shift();

    return (
        <Link href={`/event/${id}`}>
            <a className={classNames([styles.holder, styles[`holder--${status}`]])}>
                <div className={styles.cover}>
                    <img src={cover} alt={title} />
                    <span className={styles.price}>от {firstTicket?.price} ₽</span>
                </div>
                <div className={styles.header}>
                    <h2 className={styles.header__title}>{title}</h2>
                </div>
                <ul className={styles.meta}>
                    <li className={styles.meta__item}>{moment(start_at).locale('ru').format('LL hh:mm')}</li>
                    <li className={styles.meta__item}>{location.address}</li>
                    {/* <li className={styles.meta__item}>Осталось {places_remaining} билета</li> */}
                </ul>
            </a>
        </Link>
    )
}