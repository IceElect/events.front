import React from 'react'
import classNames from 'classnames'
import Link from 'next/link'

const Button = (props, ref) => {
    let classes = classNames({
        'button': true,

        'button--main': (props.theme === 'main'),
        'button--primary': (props.theme === 'primary'),
        'button--transparent': (props.theme === 'transparent'),

        'button--full': (props.full),

        'button--small': (props.size === 'small'),
        'button--large': (props.size === 'large'),

        'button--icon': (props.icon && !props.children),
        'button--bordered': (!!props.bordered),
        'button--outline-main': (props.outline === 'main'),
        'button--outline-primary': (props.outline === 'primary'),
        'button--icon-right' : (props.iconPos === 'right'),
        'active' : (props.active)
    }, props.className || []);
    let styles = props.style ? props.style : {};

    let attr = {
        href: props.href ?? '',
        ref: ref,
        to: props.to,
        className: classes,
        onClick: props.onClick,
        disabled: props.disabled,
        style: {...styles, color: props.color},
        "data-tip": props.tooltip ? props.tooltip : false,
        htmlFor: props.htmlFor
    };

    let icon = props.icon ? React.createElement('i', {className: `fal fa-fw fa-${props.icon}`, key: 'i'}) : '';
    let text = props.children ? React.createElement('span', {key: 'span'}, props.children) : '';
    let content = <>{props.iconPos === 'right' ? [text, icon] : [icon, text]}</>;

    if(props.loading) {
        content = <i className="fal fa-fw fa-spinner fa-spin" />
        // attr.disabled = "disabled"
    }

    let tag = props.to ? Link : 'button';
    if(props.as) tag = props.as;

    return React.createElement( tag, attr, content);
}

export default React.forwardRef(Button)