import Button from "../UI/Button";
import styles from "./BoxNew.module.scss";

export type BoxNewProps = {
    title: string;
    thumbnail: string;
    short: string;
    date: number|string;
    source: string;
}

export default function BoxNew({title, thumbnail, short, date, source}: BoxNewProps) {
    return (
        <article className={styles.holder}>
            <div className={styles.thumb}>
                <img src={thumbnail} alt={title} />
            </div>
            <span className={styles.source}>{source}</span>
            <span className={styles.date}>{date}</span>
            <h3 className={styles.title}>{title}</h3>
            <div className={styles.text}>{short}</div>
            <Button full={true} className={styles.footer__button}>Подробнее</Button>
        </article>
    )
}