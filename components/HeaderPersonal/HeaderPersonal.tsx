import classNames from "classnames";
import Link from "next/link";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { logout } from "../../store/user.slice";
import Button from "../UI/Button";
import styles from "./HeaderPersonal.module.scss";

export default function HeaderPersonal({isLogged, user}) {

    const dispatch = useDispatch();

    const [opened, setOpened] = useState(false);

    const toggledOpened = () => setOpened(opened => !opened);
    console.log('isLogged', isLogged);

    return (
        <div className={styles.holder}>
            {isLogged 
                ? <>
                    <a onClick={toggledOpened} className={`${styles.avatar} header__circle`}>
                        {/* <span className="header__personal-avatar-mock"><UserIcon/></span> */}
                        <span className={`${styles.avatar__sign}`}>A</span>
                    </a>
                    <ul className={classNames({
                        [styles.dropdown]: true,
                        [styles['dropdown--opened']]: opened
                    })}>
                        <li className={styles.dropdown__item}>
                            {/* <a href="#" className={classNames(styles.dropdown__profile, styles.dropdown__link)}>123</a> */}
                            <Link href="/profile">
                                <a href="#" className={styles.dropdown__link}>Мой профиль</a>
                            </Link>
                            <Link href="/tickets">
                                <a href="#" className={styles.dropdown__link}>Мои покупки</a>
                            </Link>
                            <span onClick={() => dispatch(logout())} className={styles.dropdown__link}>Выход</span>
                        </li>
                    </ul>
                </>
                : <Link href="/auth">
                    <a className={`${styles.avatar} header__circle`}>
                        {/* <span className="header__personal-avatar-mock"><UserIcon/></span> */}
                        <span className={styles.avatar__mock}><i className="fal fa-user"/></span>
                    </a>
                </Link>
            }
        </div>
    )
}