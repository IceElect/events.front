import Control from "./UI/Control";
import Button from "./UI/Button";
import styles from "../styles/auth.module.scss";
import InputMask from 'react-input-mask';
import { useForm, useFormState } from "react-hook-form";

export default function AuthOtpVerify({phone, setVerify, resend, duration, error, onSubmit}) {

    const { register, handleSubmit, control } = useForm({
        mode: 'all',
        reValidateMode: 'onBlur',
    });
    const { isSubmitting, isValid, errors } = useFormState({control});

    return (
        <form onSubmit={handleSubmit(onSubmit)} className={styles.authForm}>
            <h1 className={styles.header}>
                <span>{phone}</span>
                <span className={styles.header__edit} onClick={() => setVerify(false)}><i className="fas fa-pencil"/></span>
            </h1>
            <p className={styles.header__desc}>Мы отправили код в содержании смс на указанный номер телефона</p>
            <Control 
                name="code"
                as={InputMask} 
                mask="9999" 
                maskChar={null}
                placeholder="Код из СМС"
                {...register("code", { required: true, minLength: 4 })} 
                error={errors?.code?.message || error}
            />
            <Button full={1} theme="main" disabled={!isValid} loading={isSubmitting}>Войти в аккаунт</Button>
            {
                duration > 0
                    ? <div className={styles.resend}>Отправить код повторно через {duration} секунд.</div>
                    : <a onClick={resend} className={`${styles.resend} ${styles.resend__link}`} href="#">Отправить код повторно</a>
            }
        </form>
    )
}