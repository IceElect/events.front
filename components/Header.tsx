import React from "react";
import Link from "next/link";
import HeaderPersonal from "./HeaderPersonal/HeaderPersonal";
import UserIcon from "../assets/fontawesome/svgs/light/user.svg"
import useUser from "../lib/hooks/useUser";

export default function Header() {

    const { user, isLogged, isLoading } = useUser();
    
    return (
        <header className="header">
            <Link href="/">
                <div className="header__logo header__circle">
                    <img src="/logo.png" alt="SL,Events" />
                </div>
            </Link>
            <HeaderPersonal user={user} isLogged={isLogged} />
        </header>
    );
}