import styles from './Loader.module.scss';

export default function Loader() {
    return (
        <div className={styles.holder}>
            <img src="/logo.png" alt="SL,Events" />
        </div>
    )
}