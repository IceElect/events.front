import { useEffect } from "react";
import { useDispatch } from "react-redux";
import useUser from "../lib/hooks/useUser";
import { fetchProfile } from "../store/user.slice";
import Loader from "./Loader/Loader";

export default function LoadingLayout({children}) {
    const { user, isLogged, isLoading } = useUser();
    const dispatch = useDispatch();
    
    useEffect(() => {
        if(user === null)
            dispatch(fetchProfile());
    }, [])
    
    if(isLoading || user === null) return <Loader />;

    return children;
}