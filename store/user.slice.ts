import nookies from 'nookies'
import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import type { RootState } from '.';
import { getMyTickets, getProfile, updateProfile } from '../lib/api';

export type User = {
    id?: number;
    phone?: string;
    email?: string;
    first_name?: string;
    last_name?: string;
}

export type AuthResultOld = {
    success: boolean;
    message?: string;
    isExists: boolean;
    tokenType: string;
    accessToken: string;
    refreshToken: string;
    expiresIn: number;
}

export type AuthResult = {
    success: boolean;
    token_type: string;
    access_token: string;
    expires_at: string;
    user: object;
}

export type UserState = {
    user: any;
    phone: string;
    tickets: any;
    isLogged: boolean;
    isLoading: boolean;
    error: string;
};

const initialState: UserState = {
    user: null,
    tickets: null,
    phone: null,
    isLogged: false,
    isLoading: true,
    error: null,
};

// Async Actions

const authWithPhone = createAsyncThunk('user/authWithPhone', async (payload: string) => {

});

const authProcess = createAsyncThunk('user/authProcess', async (payload: AuthResult, thunkApi) => {
    if(payload.success) {
        return payload;
        // thunkApi.dispatch(fetchProfile());
    } else {
        return thunkApi.rejectWithValue('Unauthorized');
    }
});

const fetchProfile = createAsyncThunk('user/fetchProfile', async (payload, thunkApi) => {
    const data : any = await getProfile();
    if(data) {
        return data;
    } else {
        // localStorage.removeItem('token');
        return thunkApi.rejectWithValue('Error');
    }
});

const fetchTickets = createAsyncThunk('user/fetchTickets', async (payload, thunkApi) => {
    const data : any = await getMyTickets();
    if(data) {
        return data;
    } else {
        // localStorage.removeItem('token');
        return thunkApi.rejectWithValue('Error');
    }
});

const editProfile = createAsyncThunk('user/editProfile', async (payload: User, thunkApi) => {
    const data : any = await updateProfile(payload);
    if(data) {
        if('errors' in data) {
            return thunkApi.rejectWithValue(data.errors[Object.keys(data.errors)[0]])
        }
        return data;
    } else {
        console.warn(data);
        return thunkApi.rejectWithValue('Error');
    }
});

const logout = createAsyncThunk('user/logout', async (payload, thunkApi) => {
    // localStorage.removeItem('token');
    nookies.destroy(null, 'token')
    return true;
})

// Slice
  
export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(authProcess.pending, (state) => {
                state.error = null;
                // state.isLoading = true;
            })
            .addCase(authProcess.fulfilled, (state, {payload} : {payload: any}) => {
                // localStorage.setItem('token', payload.access_token);
                nookies.set(null, 'token', payload.access_token);

                state.user = payload.user;
                state.isLogged = true;
                // state.isLoading = false;
            })
            .addCase(authProcess.rejected, (state, {payload}) => {
                state.error = "Неверный код телефона";
                // state.isLoading = false;
            })
            .addCase(fetchProfile.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(fetchProfile.fulfilled, (state, {payload}) => {
                state.isLoading = false;
                if (!('message' in payload)) {
                    state.user = <object>payload;
                    state.isLogged = true;
                } else {
                    state.user = false;
                }
            })
            .addCase(fetchProfile.rejected, (state, {payload}) => {
                state.user = false;
                state.isLogged = false;
                state.isLoading = false;
                state.error = undefined;
            })
            .addCase(fetchTickets.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(fetchTickets.fulfilled, (state, {payload}) => {
                state.isLoading = false;
                state.tickets = payload.data;
            })
            .addCase(fetchTickets.rejected, (state, {payload}) => {
                state.isLoading = false;
                state.tickets = [];
            })
            .addCase(logout.fulfilled, (state) => {
                state.user = false;
                state.isLogged = false;
            })
            .addCase(editProfile.fulfilled, (state, {payload}: {payload: User}) => {
                state.user = payload;
            })
            .addCase(editProfile.rejected, (state, {payload}) => {
                state.error = <string>payload;
            });
    }

});

// Exports

export const { actions, reducer } = userSlice;

export const getUser = (state: RootState) => state.user.user;
export const getTickets = (state: RootState) => state.user.tickets;
export const getError = (state: RootState) => state.user.error;
export const getIsLogged = (state: RootState) => state.user.isLogged;
export const getIsLoading = (state: RootState) => state.user.isLoading;

export {
    logout,
    authProcess,
    fetchProfile,
    fetchTickets,
    editProfile,
}

export default reducer;